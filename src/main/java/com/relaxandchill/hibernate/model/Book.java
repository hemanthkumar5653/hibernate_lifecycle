package com.relaxandchill.hibernate.model;

import javax.persistence.*;

@Entity
@Table(name = "book")
public class Book {
	
    private String author;
    private float price;
  
    @Id
    @Column(name = "book_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
   private int id;
    
    @Column(name="book_title")
    private String title;
    
    public Book(String author, float price, String title) {
		super();
		this.author = author;
		this.price = price;
		this.title = title;
	}

	public long getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getTitle() {
        return title;
    }
 
    public void setTitle(String title) {
        this.title = title;
    }
 
    public String getAuthor() {
        return author;
    }
 
    public void setAuthor(String author) {
        this.author = author;
    }
 
    public float getPrice() {
        return price;
    }
 
    public void setPrice(float price) {
        this.price = price;
    }

	@Override
	public String toString() {
		return "Book [author=" + author + ", price=" + price + ", id=" + id + ", title=" + title + "]";
	}
   
 
}