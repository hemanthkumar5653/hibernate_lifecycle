package com.relaxandchill.hibernate.client;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import com.relaxandchill.hibernate.model.Book;

public class Client {
	public static void main(String args[]) {
	SessionFactory sessionFactory = new Configuration()
            .configure("hibernate-cfg.xml").buildSessionFactory();

    //2. Create the session from sessionfactory
    Session session = sessionFactory.openSession();

    //3. Begin the transaction
    Transaction transaction = session.beginTransaction();

        Book book = new Book("stephen",222,"lol");
   
    try {
        //4. call the save method on session passing the user
        session.save(book);
        transaction.commit();
    }
    catch (Exception exception){
        //5. In case of exception rollback the transaction
    	exception.printStackTrace();
        transaction.rollback();
    } 
    finally{
        session.close();
    }

}}
